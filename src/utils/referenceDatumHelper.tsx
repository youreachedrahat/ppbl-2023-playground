import { GraphQLInlineDatum } from "../types/cardanoGraphQL"
import { hexToString } from ".";

export declare type ContributorReferenceDatum = {
    luckyNumber: number;
    completedModules: string[];
  };

const formatContributorReferenceDatum = (refUTxODatum: GraphQLInlineDatum) => {
    const completedModulesList: string[] = [];

    if (refUTxODatum.value.fields[1].list.length > 0) {
        refUTxODatum.value.fields[1].list.map((module: { bytes: string }) => {
        completedModulesList.push(hexToString(module.bytes));
        });
    }

    const _contributor: ContributorReferenceDatum = {
        luckyNumber: refUTxODatum.value.fields[0].int,
        completedModules: completedModulesList,
    };

    return _contributor;
    };

  export const getContributorReferenceDatum = (datum: GraphQLInlineDatum): ContributorReferenceDatum | undefined => {
  
    if(datum?.value){
      const _formatted_datum = formatContributorReferenceDatum(datum);
      return _formatted_datum;
    }
    else {
      return undefined
    }
  };