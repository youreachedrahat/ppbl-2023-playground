import Head from 'next/head';
import { Box, Divider, Link as CLink, Heading, Text } from '@chakra-ui/react';
import {
	Table,
	Thead,
	Tbody,
	Tfoot,
	Tr,
	Th,
	Td,
	TableCaption,
	TableContainer,
} from '@chakra-ui/react';
import { CardanoWallet } from '@meshsdk/react';
import { useAssets, useWallet } from '@meshsdk/react';
import { useLovelace } from '@meshsdk/react';
import { ContributorComponent } from '@/src/components/ContributorComponent';
import { QueryComponent } from '@/src/components/QueryComponent';

export default function StudentTemplate() {
	const { connected } = useWallet();
	const assets = useAssets();
	const lovelace = useLovelace();

	return (
		<>
			<Head>
				<title>PPBL 2023 Playground</title>
				<meta
					name='description'
					content='Plutus Project-Based Learning from Gimbalabs'
				/>
				<meta name='viewport' content='width=device-width, initial-scale=1' />
				<link rel='icon' href='/favicon.ico' />
			</Head>
			<Divider w='70%' mx='auto' pb='10' />
			<Box w={['100%', '70%']} mx='auto' my='10'>
				<Box mx='auto' my='5' p='5' border='1px' borderRadius='md'>
					<Heading my='5' p='5' mx='auto'>
						Wallet assets
					</Heading>
					{lovelace ? (
						<TableContainer my='5' p='5' mx='auto'>
							<Table variant='striped' size='sm' colorScheme='blackAlpha'>
								<Thead>
									<Tr>
										<Th>Ada ₳</Th>
									</Tr>
								</Thead>
								<Tbody>
									<Tr>
										<Td>₳ {parseInt(lovelace) / 1000000}</Td>
									</Tr>
								</Tbody>
							</Table>
						</TableContainer>
					) : (
						<TableContainer>
							<Table>
								<Thead>
									<Tr>
										<Th>Ada</Th>
									</Tr>
								</Thead>
								<Tbody>
									<Tr>
										<Td>₳ 0</Td>
									</Tr>
								</Tbody>
							</Table>
						</TableContainer>
					)}
					<TableContainer my='5' p='5' mx='auto'>
						<Table variant='striped' size='sm' colorScheme='blackAlpha'>
							<Thead>
								<Tr>
									<Th>Asset Name</Th>
									<Th>Quantity</Th>
								</Tr>
							</Thead>
							<Tbody>
								{assets &&
									assets.map((asset, i) => {
										return (
											<Tr key={i}>
												<Td>{asset.assetName}</Td>
												<Td>{asset.quantity}</Td>
											</Tr>
										);
									})}
							</Tbody>
						</Table>
					</TableContainer>
					<Heading size='md' pb='3'>
						Checking your PPBL 2023 Contributor Token:
					</Heading>
					<div style={{ position: 'absolute', top: 0, right: 0 }}>
						<CardanoWallet />
					</div>
					<ContributorComponent />
					<QueryComponent />
				</Box>
			</Box>
		</>
	);
}
